How to Use 
==========

* There are 2 scripts `aflcsv2json` and `nrlcsv2json` 
* Before you run the afl script, save the excel report as 'afl.csv' in the same same directory as  `aflcsv2json.py`
* Before you run the nrl script, save the excel report as 'nrl.csv' in the same same directory as  `nrlcsv2json.py`
* If you do not redirect output, the both scripts will output to screen
* To run script do `python csv2json.py`
* To create the JSON file do `python aflcsv2json.py > afl.json`  

Easy Use
========
* Download CSV files to the same directory as the `run.sh` script
* Ensure you save both CSV files as `afl.csv` and `nrl.csv`
* You can now just execute `run.sh` which will create the `afl.json` and `nrl.json` files.
* Upon successfull creation of the JSON files it will delete the CSV files.


Warning
=======

* The time converstion to zulu time is manual atm. The script will be need to modified when Day light saving is on/off.


To-Do
=====

* Automate time convertion to handle Day light savings
* Handle the Ooyala API call