import csv
from sys import exit
from datetime import datetime
import jsontemplate
from pprint import pprint # for debugging

def convert_to_zulu_time(time, start=''):

	time_diff = 10

	if ':' in time:
		split_time =  time.split(':')
		split_time[0] =  str(int(split_time[0]) - time_diff) 
		split_time[0] =  split_time[0].zfill(2) #adding a leading 0
		return ':'.join(split_time)
		
	else:
	# if the end time is 'n/a' set the end time to 3 hours after start time
		set_end_time = start.split(':')
		set_end_time[0] = str(int(set_end_time[0]) + 3)
		set_end_time[0] =  set_end_time[0].zfill(2) #adding a leading 0
		return ':'.join(set_end_time)


def get_game_data(game):
	state = ''
	return '  {' + "\n" + ' 	"name":' + '"' + season_round + ' - ' + contents[game]['home'] + ' v ' + contents[game]['away'] + ' - ' + contents[game]['broadcaster'] + ' ' +state +'",' +  "\n" + ' 	"startTime": ' + '"' + contents[game]['date'] + 'T' + contents[game]['live stream commences'] + 'z' +'",' 	+ "\n" + ' 	"endTime": ' + '"' + contents[game]['date']+ 'T' +contents[game]['live stream ends'] + 'z' +'"' + "\n" 


def construct_game_data(game,last_game):

	game_data = get_game_data(game)
	if game == last_game:
		return game_data + '  }'
	else:
		return game_data + '  },'


# open .csv file
filename = 'afl.csv'
reader = csv.reader(open(filename,'rU'),dialect='excel')

# required fields from row array
date = 0
game = 1
home = 2
away = 4
broadcaster = 6
stream_start = 13
stream_ends = 15

#other
season_round = ''
count = 0
contents  = {}


content_providers = {}

for row in reader:
# ignore headings
 if count == 0:
 	for item in row:
 		if 'SEASON' in item:
 			season_round = item	

	
 if 'Date' in row: count=1; continue 
 if count:
 	 
 	 # ignore empty lines (csv has empty lines)
 	 # create dictionary items with game number as the key since it is  unique
 	 # remove the day from date and change the date format to YY-mm-dd
	 	
	 	if row[game]:

	 		# create a dictionary with the key as broadcaster for later use

	 		if row[broadcaster] not in content_providers:
	 			content_providers[row[broadcaster]] = []

		 	contents[row[game]] = {
			 	'game': row[game],
			 	'date': datetime.strptime(row[date].split(' ')[1], "%d/%m/%y").strftime('%Y-%m-%d'), 
			 	'home': row[home], 
			 	'away': row[away], 
			 	'broadcaster': row[broadcaster],
			 	'live stream commences': convert_to_zulu_time(row[stream_start]), 
			 	'live stream ends': convert_to_zulu_time(row[stream_ends], convert_to_zulu_time(row[stream_start]))
		 	}



# group all games that belong to a broadcaster in a list

for game in contents:
	 	content_providers[contents[game]['broadcaster']].append(contents[game]['game'])


	 	
# find which broadcaster does each game belong to 
 
# for game in contents:

# 	for provider,games in content_providers.iteritems():
# 	 if contents[game]['game'] in games:
# 	 	print contents[game]['game'],' ',provider 



states = ['VIC','QLD','SA','WA','NSW']

# contructing json output with game date #

print jsontemplate.afl_HEADER

for provider, games in content_providers.iteritems():
	
	if provider != 'Channel 7':
		if provider == content_providers.keys()[-1]:
			afl_provider_end = ' ] \n}'
		else:
			afl_provider_end = ' ] \n},'


	if provider == 'Channel 7':		
	
		for state in states:
			
			if state != states[-1]:
				afl_CH7_end =  '] \n},'
			
			if state == states[-1]:
				afl_CH7_end =  '] \n}'


			if state == 'VIC':
				print jsontemplate.afl_ch7_VIC_start	

				for game in games:		
					print construct_game_data(game,games[-1])	

				print afl_CH7_end			
				

			if state == 'QLD':
				print jsontemplate.afl_ch7_QLD_start	
				
				for game in games:
					print construct_game_data(game,games[-1])		
				
				print afl_CH7_end	
				

			if state == 'SA':
				print jsontemplate.afl_ch7_SA_start	
				
				for game in games:
				 	print construct_game_data(game,games[-1])	

				print afl_CH7_end									


			if state == 'WA':
				print jsontemplate.afl_ch7_WA_start	

				for game in games:
					print construct_game_data(game,games[-1])	 
				
				print afl_CH7_end	

			if state == 'NSW':
				print jsontemplate.afl_ch7_NSW_start	
				
				for game in games:
					print construct_game_data(game,games[-1])	

				#print jsontemplate.afl_ch7_NSW_end							
				print afl_CH7_end	


 	elif provider == 'Fox Footy':
 		print jsontemplate.afl_FOX_FOOTY_start
 		
 		for game in games:
 			print construct_game_data(game,games[-1])	

		#print jsontemplate.afl_FOX_FOOTY_end		
		print afl_provider_end		

	# elif provider == 'Fox Sports 3':



print jsontemplate.afl_FOOTER