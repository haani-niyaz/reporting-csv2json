#! /bin/bash

python aflcsv2json.py > afl.json

if [ $? -eq 0 ]; then
	rm afl.csv
fi

python nrlcsv2json.py > nrl.json

if [ $? -eq 0 ]; then
	rm nrl.csv
fi