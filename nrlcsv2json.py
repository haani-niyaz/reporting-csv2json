import csv
from sys import exit
from datetime import datetime
import jsontemplate
from pprint import pprint # for debugging

def convert_to_zulu_time(time):

	time_diff = 2 #forward

	time = time.split(' ')[0] # remove AM/PM from time string
	split_time =  time.split(':')
	split_time[0] =  str(int(split_time[0]) + time_diff) 
	split_time[0] =  split_time[0].zfill(2) #adding a leading 0
	return ':'.join(split_time)
	

def get_match_encoder(encoder):

	return encoder.split('-')[2]


def get_game_data(game_round):

	return  '  {' + "\n" + ' 	"name":' + '"' + contents[game_round]['round'].split('-')[0]  +' - ' + contents[game_round]['home'] + ' v ' + contents[game_round]['away'] + ' - ' + contents[game_round]['round'].split('-')[1] + ' ' +'",' +  "\n" + ' 	"startTime": ' + '"' + contents[game_round]['date'] + 'T' + contents[game_round]['stream starts'] + 'z' +'",' 	+ "\n" + ' 	"endTime": ' + '"' + contents[game_round]['date']+ 'T' +contents[game_round]['stream ends'] + 'z' +'"' + "\n" 


def construct_game_data(game_round,last_round):

	 game_data = get_game_data(game_round)
	 if game_round == last_round: 
	 	return game_data + '  }' 
	 else: 
	 	return game_data + '  },'

def dd(stuff):
	pprint(stuff)
	print '\n\n\n'


# open .csv file
filename = 'nrl.csv'
reader = csv.reader(open(filename,'rU'),dialect='excel')

# required fields from row array

date = 0
game_round = 1
home = 2
away = 4
encoder = 9
stream_start = 14
stream_end = 17

# other
count = 0
contents = {}
encoders = {}



for row in reader:
	if 'Date' in row: count=1; continue
	if count:

		# ignore empty lines (csv has empty lines)
		# create dictionary items with game_round number as the key since it is  unique

		if row[game_round]:

			# create a dictionary with the key as broadcaster for later use

			if row[encoder] not in encoders:
			 encoders[get_match_encoder(row[encoder])] = []
								

			contents[row[game_round]] = {
				'round'			  : row[game_round],
				'date'			  : datetime.strptime(row[date].split(' ')[1], "%d/%m/%y").strftime('%Y-%m-%d'),
				'home'			  : row[home],
				'away'			  : row[away],
				'encoder'		  : get_match_encoder(row[encoder]),
				'stream starts': convert_to_zulu_time(row[stream_start]),
				'stream ends'  : convert_to_zulu_time(row[stream_end])
			}


# group all games that belong to a broadcaster in a list

for game in contents:
	encoders[contents[game]['encoder']].append(contents[game]['round'])


print jsontemplate.nrl_HEADER

for encoder, games in encoders.iteritems():
	if encoder == encoders.keys()[-1]:
		nrl_encoder_end = ' ] \n}'
	else:
		nrl_encoder_end = ' ] \n},'


	if encoder == 'Match1':
		print jsontemplate.nrl_MATCH1_start

		for game_round in games:
			print construct_game_data(game_round,games[-1])

		print nrl_encoder_end

	if encoder == 'Match2':
		print jsontemplate.nrl_MATCH2_start

		for game_round in games:
			print construct_game_data(game_round,games[-1])

		print nrl_encoder_end


	if encoder == 'Match3':
		print jsontemplate.nrl_MATCH3_start

		for game_round in games:
			print construct_game_data(game_round,games[-1])

		print nrl_encoder_end


	if encoder == 'Press1':
		print jsontemplate.nrl_PRESS1_start

		for game_round in games:
			print construct_game_data(game_round,games[-1])

		print nrl_encoder_end


	if encoder == 'Press2':
		print jsontemplate.nrl_PRESS2_start

		for game_round in games:
			print construct_game_data(game_round,games[-1])

		print nrl_encoder_end


	if encoder == 'Press3':
		print jsontemplate.nrl_PRESS3_start

		for game_round in games:
			print construct_game_data(game_round,games[-1])

		print nrl_encoder_end


	if encoder == 'Overflow1':
		print jsontemplate.nrl_OVERFLOW1_start

		for game_round in games:
			print construct_game_data(game_round,games[-1])

		print nrl_encoder_end


	if encoder == 'Overflow2':
		print jsontemplate.nrl_OVERFLOW2_start

		for game_round in games:
			print construct_game_data(game_round,games[-1])

		print nrl_encoder_end			
			

print jsontemplate.nrl_FOOTER

	
			

			

